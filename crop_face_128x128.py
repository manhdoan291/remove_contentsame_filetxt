import copy
import argparse
import os
import cv2 as cv
import time
import numpy as np
import mediapipe as mp
import onnxruntime

from utils.cvfpscalc import CvFpsCalc

SAVE_IMAGE = '/home/xteam/Documents/Doan/FaceDetection-Anti-Spoof-Demo/crop_result/'

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--fd_model_selection", type=int, default=0)
    parser.add_argument(
        "--min_detection_confidence",
        help='min_detection_confidence',
        type=float,
        default=0.7,
    )

    parser.add_argument(
        "--as_input_size",
        type=str,
        default='128,128',
    )

    args = parser.parse_args()

    return args


def run_face_detection(
        face_detection,
        image,
        expansion_rate=[0.1, 0.4, 0.1, 0.0],  # x1, y1, x2, y2
):
    # preprocessing:BGR->RGB
    input_image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

    # inference
    results = face_detection.process(input_image)

    # post-processing
    image_width, image_height = image.shape[1], image.shape[0]
    bboxes = []
    keypoints = []
    scores = []
    if results.detections is not None:
        for detection in results.detections:
            # bounding box
            bbox = detection.location_data.relative_bounding_box
            x1 = int(bbox.xmin * image_width)
            y1 = int(bbox.ymin * image_height)
            w = int(bbox.width * image_width)
            h = int(bbox.height * image_height)
            x1 = x1 - int(w * expansion_rate[0])
            y1 = y1 - int(h * expansion_rate[1])
            x2 = x1 + w + int(w * expansion_rate[0]) + int(
                w * expansion_rate[2])
            y2 = y1 + h + int(h * expansion_rate[1]) + int(
                h * expansion_rate[3])

            x1 = np.clip(x1, 0, image_width)
            y1 = np.clip(y1, 0, image_height)
            x2 = np.clip(x2, 0, image_width)
            y2 = np.clip(y2, 0, image_height)

            bboxes.append([x1, y1, x2, y2])

            # keypoint: right eye
            keypoint0 = detection.location_data.relative_keypoints[0]
            keypoint0_x = int(keypoint0.x * image_width)
            keypoint0_y = int(keypoint0.y * image_height)
            # keypoint: left eye
            keypoint1 = detection.location_data.relative_keypoints[1]
            keypoint1_x = int(keypoint1.x * image_width)
            keypoint1_y = int(keypoint1.y * image_height)
            # keypoint: nose
            keypoint2 = detection.location_data.relative_keypoints[2]
            keypoint2_x = int(keypoint2.x * image_width)
            keypoint2_y = int(keypoint2.y * image_height)
            # keypoint: mouth
            keypoint3 = detection.location_data.relative_keypoints[3]
            keypoint3_x = int(keypoint3.x * image_width)
            keypoint3_y = int(keypoint3.y * image_height)
            # keypoint: right ear
            keypoint4 = detection.location_data.relative_keypoints[4]
            keypoint4_x = int(keypoint4.x * image_width)
            keypoint4_y = int(keypoint4.y * image_height)
            # keypoint: left ear
            keypoint5 = detection.location_data.relative_keypoints[5]
            keypoint5_x = int(keypoint5.x * image_width)
            keypoint5_y = int(keypoint5.y * image_height)

            keypoints.append([
                [keypoint0_x, keypoint0_y],
                [keypoint1_x, keypoint1_y],
                [keypoint2_x, keypoint2_y],
                [keypoint3_x, keypoint3_y],
                [keypoint4_x, keypoint4_y],
                [keypoint5_x, keypoint5_y],
            ])

            # score
            scores.append(detection.score[0])
    return bboxes, keypoints, scores

def main():

    args = get_args()
    
    path_image = "/home/xteam/Documents/Doan/FaceDetection-Anti-Spoof-Demo/sum_live1/"

    for image_index in os.listdir(path_image):
        image_name = path_image + image_index
        image = cv.imread(image_name)
        
        fd_model_selection = args.fd_model_selection
        min_detection_confidence = args.min_detection_confidence
        as_input_size = [int(i) for i in args.as_input_size.split(',')]
        # face-detection
        mp_face_detection = mp.solutions.face_detection
        face_detection = mp_face_detection.FaceDetection(
            model_selection=fd_model_selection,
            min_detection_confidence=min_detection_confidence,
        )
        # spoofing detection
        # detection implementation #############################################################
        start_time = time.time()
        bboxes, keypoints, scores = run_face_detection(face_detection, image)
        # spoofing detection
        for bbox in bboxes:
            crop_img = image[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            print(image_name)
            dim = (128, 128)
            # resize image
            crop_resized = cv.resize(crop_img, dim, interpolation = cv.INTER_AREA)
            cv.imwrite(SAVE_IMAGE+image_index,crop_resized)

if __name__ == '__main__':

    main()
    
