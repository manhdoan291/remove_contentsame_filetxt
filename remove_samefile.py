# so sanh  small_file(file thua) va long_file(toan bo file)
# neu khac nhau thi xoa nhung file thua trong long_file(toan bo file)
small_file = open('/home/doan19/Music/intra_test/train_label.txt','r')
long_file = open('/home/doan19/Music/intra_test/output_locfilekhac.txt','r')
output_file = open('/home/doan19/Music/intra_test/output_file.txt','w')

small_lines = small_file.readlines()
long_lines = long_file.readlines()
small_lines_cleaned = [line.rstrip().lower() for line in small_lines]
long_file_lines = long_file.readlines()
long_lines_cleaned = [line.rstrip().lower() for line in long_lines]
for line in small_lines_cleaned:
    if line not in long_lines_cleaned:
        output_file.writelines(line + '\n')

